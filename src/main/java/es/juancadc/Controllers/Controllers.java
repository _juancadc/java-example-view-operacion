package es.juancadc.Controllers;

import jakarta.security.auth.message.callback.PrivateKeyCallback.Request;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.naming.Context;

/**
 * Servlet implementation class Controllers
 */
@WebServlet(name="operar", urlPatterns = {"/operar"})
public class Controllers extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public Controllers() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String uno = request.getParameter("nUno");
		String dos = request.getParameter("nDos");
		
		Integer iUno = Integer.parseInt(uno);
		Integer iDos = Integer.parseInt(dos);
		
		String btnSuma = request.getParameter("buttonSumar");
		String btnResta = request.getParameter("buttonRestar");
		Integer resultado=0;
		
		if(btnSuma!=null)
			resultado= iUno + iDos;
		if(btnResta!=null)
			resultado= iUno - iDos;
		
		//Opcion request: solo vive durante la petición
		request.setAttribute("resultadoRequest", resultado);
		//Opcion session: vive durante la sesion 
		request.getSession().setAttribute("resultadoSesion", resultado);
		//Opcion context: Vive durante el tiempo de vida del servlet 
		getServletContext().setAttribute("resultadoContext", resultado);
		
		RequestDispatcher rd = request.getRequestDispatcher("/resultado.jsp");
		rd.forward(request, response);

	}

}
