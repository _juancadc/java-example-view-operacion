<%@ page language="java" contentType="text/html; charset=UTF-8"

    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Resultado</title>
</head>
<body>

<h1>Resultado de la operacion. Forma 1:</h1>

El resultado de la operacion con Request es:<p> ${resultadoRequest}</p>
El resultado de la operacion con Sesion es:<p> ${resultadoSesion}</p>
El resultado de la operacion con Request es:<p> ${resultadoContext}</p>

<h1>Resultado de la operacion. Forma 2:</h1>

El resultado de la operacion con Request es:<p> <%=request.getAttribute("resultadoRequest") %> </p>
El resultado de la operacion con Sesion es: <p> <%=request.getAttribute("resultadoSesion") %> </p>
El resultado de la operacion con Request es: <p> <%=request.getAttribute("resultadoContext") %> </p>

</body>
</html>